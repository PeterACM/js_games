/**
 * A simple Breakout clone
 * 
 * X and Y refer to position
 * Width and Height Refer to Size
*/


// Canvas settings
var canvas, canvasContext;

// Initial game settings
// FPS
let fps = 30;
// Ball Size and Position
let ballX = 400, ballY = 100, ballR = 10;
let ballSpeedX = 6, ballSpeedY = -8; 
// Paddle Size and Position
let paddleWidth = 100;
const paddleHeight = 10;
let paddleX = 400 - paddleWidth / 2;
let paddleY = 50;
// Brick variables
// Brick size
const brickWidth = 50;
const brickHeight = 25;
let brickGap = 2;
// Brick Grid
let brickCols = 16;
let brickRows = 12;
let brickGrid = new Array(brickCols * brickRows);
// for gap at top
let brickY = brickCols*3;
// for count
let bricksLeft = 0; // using brickGrid.length gets messed up by the top offset 'empty bricks'
// Score

// Play / Pause / Win

//Controls
let mouseX, mouseY;



//----------------------------------------------------------------------------------------------
// Initialise game
window.onload = function() {
    canvas = document.getElementById('gameCanvas');
    canvasContext = canvas.getContext('2d');

    // Initialise game
    setInterval(function() {
        moveAll();
        drawAll();
    }, 1000/fps);

    brickReset();
    ballReset();

    canvas.addEventListener('mousemove', updateMousePosition);
};


// initialise brick array
function brickReset() {
    // bricksLeft = brickGrid.length;
    let i;
    for (i = 0; i < brickY; i++) {
        brickGrid[i] = false;
    }
    for (;i < brickGrid.length; i++) {
        brickGrid[i] = true;
        bricksLeft++;
        // // Randomly set certain bricks to another value
        // // may be able to repurpose this to add power ups
        // if (Math.random() < 0.35){
        //     brickGrid[i] = true;
        // }
        // else {
        //     brickGrid[i] = special;
        // } // end of draw brick + special bricks
    } // end of for loop
}; // end of brickReset



//----------------------------------------------------------------------------------------------
// game controls
function updateMousePosition(evt) {
    // get the Canvas position relative to the web page
    // So that mouse position will be correct relative to the canvas, no matter browser window size or scroll
    let rect = canvas.getBoundingClientRect();
    let root = document.documentElement;

    // Get mouse position rfelative to canvas
    mouseX = evt.clientX - rect.left - root.scrollLeft;
    mouseY = evt.clientY - rect.top - root.scrollTop

    // set paddle X position to mouse position
    paddleX = mouseX - paddleWidth / 2;

    // hack to test ball in any position
    // ballX = mouseX;
    // ballY = mouseY;
    // ballSpeedX = 3;
    // ballSpeedY = -4;
};


// reset the ball if it goes off the bottom of the screen
function ballReset() {
    ballX = paddleX + paddleWidth/2; // canvas.width / 2;
    ballY = canvas.height - paddleY - ballR;
    if (bricksLeft == 0) {
        brickReset();
    }
};

// Movement functions
// Move the ball around the screen and check for paddle collision
function moveBall(){
    // Move the ball
    ballX += ballSpeedX;
    ballY += ballSpeedY;
    
    // Check for left edge collision
    if(ballX < 0 + ballR && ballSpeedX < 0.0) {
        // Reverse ball X direction
        ballSpeedX *= -1;
    }
    // Check for right edge collision
    if(ballX > canvas.width - ballR && ballSpeedX > 0.0) {
        // Reverse ball X direction
        ballSpeedX *= -1;
    }
    // Check for ball top edge collision
    if(ballY < 0 + ballR  && ballSpeedY < 0.0) {
        // Reverse ball Y direction
        ballSpeedY *= -1; // reverse ball's vertical direction
    }
    // Check for ball bottom edge collision 
    if(ballY > canvas.height) {
        // Lose a life
        ballReset();
    }
};


// Check for existence of a brick
function isBrickAtColRow(col, row) {
    if(col >= 0 && col < brickCols &&
        row >= 0 && row < brickRows) {

    let brickIndexUnderCoord = rowColToArrayIndex(col, row);
    return brickGrid[brickIndexUnderCoord];
    }
    else {
        return false;
    }
};


// Check for ball - brick collision
function ballBrickHandling() {
    // get the brick array index from the ball position
    let ballBrickCol = Math.floor(ballX / brickWidth);
    let ballBrickRow = Math.floor(ballY / brickHeight);
    let brickIndexUnderBall = rowColToArrayIndex(ballBrickCol, ballBrickRow);

    // check collision return is valid, i.e. ball not overlapping edge of canvas
    if(ballBrickCol >= 0 && ballBrickCol < brickCols &&
        ballBrickRow >= 0 && ballBrickRow < brickRows) {
        // check if brick exists, index value == true
        if(isBrickAtColRow(ballBrickCol, ballBrickRow)){
            // if there's a collision, remove the brick
            brickGrid[brickIndexUnderBall] = false;
            bricksLeft--;
            //console.log('bricksLeft: ', bricksLeft);

            // check direction of ball movement
            let prevBallX = ballX - ballSpeedX;
            let prevBallY = ballY - ballSpeedY;
            let prevBrickCol = Math.floor(prevBallX / brickWidth);
            let prevBrickRow = Math.floor(prevBallY / brickHeight);

            // set variable for inner corner blocking (armpit) check
            let bothTestsFailed = true;

            // check if brick side collision
            if (prevBrickCol != ballBrickCol) {
                // check if another brick would block the collision and
                // if there is no blocking brick, bounce the ball
                if(isBrickAtColRow(prevBrickCol, ballBrickRow) == false) {
                    ballSpeedX *= -1;
                    bothTestsFailed = false;
                }
            }
            // check for brick top/bottom collision
            if (prevBrickRow != ballBrickRow) {
                // check if another brick would block the collision and
                // if there are no blocking bricks, bounce the ball
                if(isBrickAtColRow(ballBrickCol, prevBrickRow) == false){
                    ballSpeedY *= -1;
                    bothTestsFailed = false;
                }
            }
            // Check for inner corner blocking (armpit)
            if (bothTestsFailed){
                ballSpeedX *= -1;
                ballSpeedY *= -1;
            }
        } // end of check brick exists true/false
    } // end of valid col/row check
};

// check for ball - paddle edge collision
function ballPaddleHandling(){
    // bounce the ball of the paddle
    let paddleTop = canvas.height - paddleY - paddleHeight;
    let paddleBottom = canvas.height - paddleY;
    let paddleLeft = paddleX;
    let paddleRight = paddleX + paddleWidth;
    
    if (
        ballY > paddleTop && ballY < paddleBottom &&
        ballX > paddleLeft && ballX < paddleRight
    ) {
        ballSpeedY *= -1;
    
        let paddleXCenter = paddleX + paddleWidth / 2;
        let ballDelta = ballX - paddleXCenter;
    
        ballSpeedX = ballDelta * 0.35;

        if (bricksLeft == 0){
            brickReset();
        } // out of bricks
    }// ball contact paddle
};


function moveAll() {
    // Ball Movement
    moveBall();

    // handle ball - brick events
    ballBrickHandling();

    // handle ball - paddle events
    ballPaddleHandling();
};


function rowColToArrayIndex (col, row) {
    return col + brickCols * row;
};


function drawBricks() {
    for (let row = 0; row < brickRows; row++) {
        for (let col = 0; col < brickCols; col++) {
            // calculate the array index of the brick
            let arrayIndex = rowColToArrayIndex(col, row);

            // if i == true draw the brick
            if (brickGrid[arrayIndex]) {
                colourRect((brickWidth*col)+1, (brickHeight * row), brickWidth - brickGap, brickHeight - brickGap, 'orange');
            } // end of draw brick grid
        } // end of collum for loop

    } // end of rows for loop
}; // end of drawbricks


function drawAll() {
    // Refresh the canvas
    colourRect(0, 0, canvas.width, canvas.height, 'black');
  
    // Draw the ball
    colourCircle(ballX, ballY, ballR, 'cyan');

    // Draw the paddle
    colourRect(paddleX, canvas.height - paddleY, paddleWidth, paddleHeight, 'white');

    // Draw the Bricks
    drawBricks();

    //for debugging
    //colourText(`${mousebrickCols},${mouseBrickRow}:${brickIndexUnderMouse}`, mouseX, mouseY, 'yellow');
};


//----------------------------------------------------------------------------------------------
//shape generics
// Rectangle
function colourRect(topLeftX,topLeftY, boxWidth,boxHeight, fillColour){
    canvasContext.fillStyle = fillColour;
    canvasContext.fillRect(topLeftX,topLeftY, boxWidth,boxHeight);
};

// Circle
function colourCircle(centerX, centerY, radius, fillColour) {
    canvasContext.fillStyle = fillColour;
    canvasContext.beginPath();
    canvasContext.arc(centerX, centerY, radius, 0, Math.PI*2, true);
    canvasContext.fill();
};

// Text
function colourText(content, textX, textY, fillcolour) {
    canvasContext.fillStyle = fillcolour;
    canvasContext.fillText(content, textX, textY);
};

