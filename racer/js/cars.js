/**
 * Peter ACM
 * Simple JS Games and other noodling
 * Car
 * 
 * X and Y refer to position
 * Width and Height Refer to Size
*/

// Car Types
// Hatchback
const carMomentum = 0.94;
const carPower = 0.5;
const carBreaks = 0.2;
const carTurning = 0.07;
const minSpeedToTurn = 0.5;
// Coupe
// Speedster
// Ute


//----------------------------------------------------------------------------------------------
// Car Size, Position and Speed
function carClass() {
    this.x;
    this.y;
    this.ang;// = -90 * Math.PI/180;
    this.speed = 0;
    this.carPic;
    this.name = "Untitled Car";

    // car controls
    this.keyHeldSpeed = false;
    this.keyHeldReverse = false;
    this.keyHeldTurnLeft = false;
    this.keyHeldTurnRight = false;

    this.controlKeyUp;
    this.controlKeyRight;
    this.controlKeyDown;
    this.controlKeyLeft;

    // bind keys
    this.setupInput = function (upKey, rightKey, downKey, leftKey) {
        this.controlKeyUp = upKey;
        this.controlKeyRight = rightKey;
        this.controlKeyDown = downKey;
        this.controlKeyLeft = leftKey;
    }

    // Car location
    this.reset = function(whichImage, carName) {
        this.carPic = whichImage; // load the correct car image
        this.name = carName;
        this.speed = 0;

        for (let row = 0; row < trackRows; row++) {
            for (let col = 0; col < trackCols; col++) {
                // calculate the array index of the track
                let arrayIndex = rowColToArrayIndex(col, row);
    
                // if i == 2 draw car
                if (trackGrid[arrayIndex] == trackPlayerStart) {
                    // replace car start '2' with track '0', so it doesn't colide with anything
                    trackGrid[arrayIndex] = trackRoad;
                    // reset car angle
                    this.ang = -Math.PI/2;
                    // set car to centre of grid location
                    this.x = col * trackWidth + trackWidth/2;
                    this.y = row * trackHeight + trackHeight/2;
                    return;
                } // end of draw car + player start if
            } // end of car location collum for loop
        } // end of car location rows for loop
        console.log("No Player Start Found");
    }

    // Move the car around the screen and check for collision
    this.move = function(){
        // slow car speed when not accelerating
        this.speed *= carMomentum;
    
        // Move the car
        if (this.keyHeldSpeed && this.speed <= 10) {
            this.speed += carPower;
        }
        if (this.keyHeldReverse && this.speed >= -6) {
            this.speed -= carBreaks;
        }
        if (Math.abs(this.speed) > minSpeedToTurn){
            if (this.keyHeldTurnLeft) {
                this.ang -= carTurning;
            }
            if (this.keyHeldTurnRight) {
                this.ang += carTurning;
            }
        }
        this.x += Math.cos(this.ang) * this.speed;
        this.y += Math.sin(this.ang) * this.speed;

        // handle car - track events
        carTrackHandling(this);
    }

    // check that car image is loaded
    this.draw = function() {
        drawBmpCenteredwithRotation(this.carPic, this.x, this.y, this.ang);
    }
};

