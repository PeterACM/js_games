/**
 * A simple Track Racer game
 * Master
 * 
 * X and Y refer to position
 * Width and Height Refer to Size
*/

// imports
// import setupInput from './js/input';
// Do imports when uploaded, CORS makes it a pain locally


// Canvas settings
var canvas, canvasContext;

let p1Car = new carClass();
let p2Car = new carClass();

// Initial game settings
// FPS
let fps = 30;
// Score
// p1Score = 0;
// p2Score = 0;
// Play / Pause / Win


//----------------------------------------------------------------------------------------------
// Initialise game
window.onload = function() {
    canvas = document.getElementById('gameCanvas');
    canvasContext = canvas.getContext('2d');

    // Create a blank background
    colourRect(0,0, canvas.width, canvas.height, 'black');
    // Loading Screen
    colourRect(200,200, 400,200, 'grey');
    colourText("JS Racer: Loading", canvas.width/2, canvas.height/2, 'white', canvasContext.textAlign = "center", canvasContext.font = 'bold 30px sans')
    
    // Load car and track images
    imageLoad();
};

// once the images have loaded, begin game
function startGame() {
    // Initialise game
    setInterval(updateAll, 1000/fps);
    
    // Initialise Game Coontrols
    setupInput();

    loadLevel(levelOne); // level one hardcoded for now, will make selectable later
};

// load the track map and cars
function loadLevel(whichLevel) {
    // load the selected map into trackGrid
    trackGrid = whichLevel.slice(); // slice copies track level array into a new array, so that the car placement to track (2 to 0) change doesn't alter the original
    // initialise car 
    p1Car.reset(p1CarPic, 'Red');
    p2Car.reset(p2CarPic, 'Green');
}


function updateAll() {
    moveAll();
    drawAll();
};


function moveAll() {
    // Car Movement
    p1Car.move();
    p2Car.move();
};


function drawAll() {
    // Draw the Tracks
    drawTracks();

    // draw the car
    p1Car.draw();
    p2Car.draw();

    //for debugging
    //colourText(`${mousetrackCols},${mouseTrackRow}:${trackIndexUnderMouse}`, mouseX, mouseY, 'yellow');
};

