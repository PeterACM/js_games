/**
 * Peter ACM
 * Simple JS Games and other noodling
 * Control input
 * 
 * X and Y refer to position
 * Width and Height Refer to Size
*/


// Controls
// Mouse
let mouseX, mouseY;

// Keyboard
// Player 1
const keyLeftArrow = 37;
const keyRightArrow = 39;
const keyUpArrow = 38;
const keyDownArrow = 40;

// Player 2
const keyW = 87;
const keyA = 65;
const keyS = 83;
const keyD = 68;

//----------------------------------------------------------------------------------------------
// game controls
// Initialise input
function setupInput() {
    canvas.addEventListener('mousemove', updateMousePosition);

    document.addEventListener('keydown', keyPressed);
    document.addEventListener('keyup', keyReleased);

    // P1 key binfdings
    p1Car.setupInput(keyW, keyD, keyS, keyA);
    p2Car.setupInput(keyUpArrow, keyRightArrow, keyDownArrow, keyLeftArrow);
};

// mouse position
function updateMousePosition(evt) {
    // get the Canvas position relative to the web page
    // So that mouse position will be correct relative to the canvas, no matter browser window size or scroll
    let rect = canvas.getBoundingClientRect();
    let root = document.documentElement;

    // Get mouse position rfelative to canvas
    mouseX = evt.clientX - rect.left - root.scrollLeft;
    mouseY = evt.clientY - rect.top - root.scrollTop
};

// keyboard controls
function keySet(evt, whichCar, setTo) {
    if (evt.keyCode === whichCar.controlKeyUp) {
        whichCar.keyHeldSpeed = setTo;
    }
    if (evt.keyCode === whichCar.controlKeyRight) {
        whichCar.keyHeldTurnRight = setTo;
    }
    if (evt.keyCode === whichCar.controlKeyDown) {
        whichCar.keyHeldReverse = setTo;
    }
    if (evt.keyCode === whichCar.controlKeyLeft) {
        whichCar.keyHeldTurnLeft = setTo;
    }
};


function keyPressed(evt) {
    // console.log(`key pressed:  ${evt.keyCode}`);
    keySet(evt, p1Car, true);
    keySet(evt, p2Car, true);
    evt.preventDefault();
};

function keyReleased(evt) {
    // console.log("key released: ", evt.keyCode);
    keySet(evt, p1Car, false);
    keySet(evt, p2Car, false);
};

