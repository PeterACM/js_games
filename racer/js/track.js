/**
 * Peter ACM
 * Simple JS Games and other noodling
 * Track
 * 
 * X and Y refer to position
 * Width and Height Refer to Size
*/


// Track variables
// Track size
const trackWidth = 40;
const trackHeight = 40;
// Track Grid
let trackCols = 20;
let trackRows = 15;
const levelOne = [4, 4, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 4,
                  4, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 4,
                  4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
                  1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1,
                  1, 0, 0, 0, 1, 1, 1, 4, 4, 4, 4, 4, 1, 1, 1, 1, 1, 0, 0, 1,
                  1, 0, 0, 1, 1, 0, 0, 1, 4, 4, 4, 1, 0, 0, 0, 0, 1, 0, 0, 1,
                  1, 0, 0, 1, 0, 0, 0, 0, 1, 4, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1,
                  1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 5, 0, 0, 1, 0, 0, 1,
                  1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1,
                  1, 0, 0, 1, 0, 0, 5, 0, 0, 0, 5, 0, 0, 1, 0, 0, 1, 0, 0, 1,
                  1, 2, 2, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 5, 0, 0, 1,
                  1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1,
                  1, 0, 3, 0, 0, 0, 1, 4, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1,
                  1, 0, 3, 0, 0, 0, 1, 4, 4, 1, 1, 1, 4, 4, 1, 0, 0, 0, 1, 4,
                  1, 1, 1, 1, 1, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 1, 1, 4, 4];
// trackGrid array to hold level
let trackGrid = [];

// Track Labels - clearer code than refering to 1, 2 etc
const trackRoad = 0;
const trackWall = 1;
const trackPlayerStart = 2;
const trackGoal = 3;
const trackTree = 4;
const trackFlag = 5;


//----------------------------------------------------------------------------------------------
// Check for existence of a track
function returnTileTypeAtColRow(col, row) {
    if(col >= 0 && col < trackCols &&
        row >= 0 && row < trackRows) {

        let trackIndexUnderCoord = rowColToArrayIndex(col, row);
        return trackGrid[trackIndexUnderCoord];
    }
    else {
        return trackWall;
    }
};


// Check for car - track wall / tree / other collision
function carTrackHandling(whichCar) {
    // get the track array index from the car position
    let carTrackCol = Math.floor(whichCar.x / trackWidth);
    let carTrackRow = Math.floor(whichCar.y / trackHeight);
    let trackIndexUnderCar = rowColToArrayIndex(carTrackCol, carTrackRow);
    
    // check collision return is valid, i.e. car not overlapping edge of canvas
    if(carTrackCol >= 0 && carTrackCol < trackCols &&
        carTrackRow >= 0 && carTrackRow < trackRows) {
            // check for tile type
            let tileHere = returnTileTypeAtColRow(carTrackCol, carTrackRow);

        if (tileHere == trackGoal){
            console.log(`${whichCar.name} Wins`)
            loadLevel(levelOne);
        }
        else if(tileHere != trackRoad){
            whichCar.x -= Math.cos(whichCar.ang) * whichCar.speed;
            whichCar.y -= Math.sin(whichCar.ang) * whichCar.speed;
            whichCar.speed *= -0.5; 
        } // end of check track exists true/false
    } // end of valid col/row check
};


// function to find the array index of a grid position
function rowColToArrayIndex (col, row) {
    return col + trackCols * row;
};


// draw the track
function drawTracks() {
    let arrayIndex = 0;
    let drawTileX = 0;
    let drawTileY = 0;

    for (let row = 0; row < trackRows; row++) {
        for (let col = 0; col < trackCols; col++) {
            arrayIndex = rowColToArrayIndex(col, row);
            let tileKindHere = trackGrid[arrayIndex];
            let useImg = trackPics[tileKindHere];

            canvasContext.drawImage(useImg, drawTileX, drawTileY);

            drawTileX +=  trackWidth;
            arrayIndex++;
        } // end of column for loop
        drawTileY += trackHeight;
        drawTileX = 0;
    } // end of rows for loop
}; // end of drawtracks

