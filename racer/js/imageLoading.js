/**
 * Peter ACM
 * Simple JS Games and other noodling
 * ImageManagement
 * 
 * X and Y refer to position
 * Width and Height Refer to Size
*/


// Car images
let p1CarPic = document.createElement("img");
let p2CarPic = document.createElement("img");
// Track images
let trackPics = [];
let picsToLoad = 0; // set automatically based on imageList in loadImages()



//----------------------------------------------------------------------------------------------
// Count down the number of images loaded and trigger gameStart
function countLoadedImages() {
    picsToLoad--;
    // console.log(picsToLoad);
    if (picsToLoad === 0) {
        startGame();
    };
};

// seperate the image loading code into its own function to reduce repetition
function beginImageLoading(imgVar, filename) {
    // call onload before .src to ensure onload is set up before the image can be loaded
    imgVar.onload = countLoadedImages;
    imgVar.src = `images/${filename}`;
};

// load track images
function loadImageForTrackCode(trackType, filename) {
    trackPics[trackType] = document.createElement("img");
    beginImageLoading(trackPics[trackType], filename);
};

// Load images
function imageLoad() {
    // ensure all images are loaded before beginning game
    let imageList = [
        // cars
        {imgVar: p1CarPic, filename: "player1Car.png"},
        {imgVar: p2CarPic, filename: "player2Car.png"},
        // track
        {trackType: trackRoad, filename: "track_road.png"},
        {trackType: trackWall, filename: "track_wall.png"},
        {trackType: trackGoal, filename: "track_goal.png"},
        {trackType: trackTree, filename: "track_tree.png"},
        {trackType: trackFlag, filename: "track_flag.png"},
    ];
    
    picsToLoad = imageList.length; // total number of individual images to be loaded

    for (let i=0; i<imageList.length; i++) {
        if(imageList[i].imgVar != undefined) {
            beginImageLoading(imageList[i].imgVar, imageList[i].filename); 
        }
        else {
            loadImageForTrackCode(imageList[i].trackType, imageList[i].filename);
        }
    }
};

