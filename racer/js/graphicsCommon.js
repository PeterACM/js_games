/**
 * Peter ACM
 * Simple JS Games and other noodling
 * Common Graphics
 * 
 * X and Y refer to position
 * Width and Height Refer to Size
*/


// bitmap move and rotate
function drawBmpCenteredwithRotation(useBmp, atX, atY, withAng) {
    canvasContext.save();
    canvasContext.translate(atX, atY);
    canvasContext.rotate(withAng);
    canvasContext.drawImage(useBmp, -useBmp.width/2, -useBmp.height/2);
    canvasContext.restore();
};

//----------------------------------------------------------------------------------------------
// shape generics
// Rectangle
function colourRect(topLeftX,topLeftY, boxWidth,boxHeight, fillColour){
    canvasContext.fillStyle = fillColour;
    canvasContext.fillRect(topLeftX,topLeftY, boxWidth,boxHeight);
};

// Circle
function colourCircle(centerX, centerY, radius, fillColour) {
    canvasContext.fillStyle = fillColour;
    canvasContext.beginPath();
    canvasContext.arc(centerX, centerY, radius, 0, Math.PI*2, true);
    canvasContext.fill();
};

// Text
function colourText(content, textX, textY, fillcolour) {
    canvasContext.fillStyle = fillcolour;
    canvasContext.fillText(content, textX, textY);
};

