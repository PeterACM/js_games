/**
 * Simple Air Hockey JS Game
 * Game
 * 
 * X and Y used for position
 * Width and Height used for size
 * Numbers 1 and 2 to distinguish player 1 and 2
*/

// Canvas Settings
var canvas;
var canvasContext;

// Initial Game Settings
// FPS
let fps = 30;
// Goal Size
let goalHeight = 160;
// Paddles Size
let pad1Height = 100;
let pad2Height = 100;
const padWidth = 20;
// Paddle Position
let padOffset = 30;
let pad1Y = 310 - pad1Height / 2;
let pad2Y = 310 - pad2Height / 2;
// Ball Size and Position
let ballR = 7;
let ballX = padOffset + padWidth + ballR + 1;
let ballY = pad1Y + (pad1Height / 2) ;
let ballSpeedX = -10;
let ballSpeedY = 0;
// Score
let p1Score = 0;
let p2Score = 0;
const winningScore = 2;
// Play / Pause / Win
let showingWinScreen = false;


// Load canvas details from HTML
window.onload = function() {
    canvas = document.getElementById('gameCanvas');
    canvasContext = canvas.getContext('2d');

    // Listen for mousemove events to move the paddle
    canvas.addEventListener('mousemove', handleMouseMove);

    // Listen for mouse click
    canvas.addEventListener('mousedown', handleMouseClick);

    // Initialise the game
    drawCourt();
    drawPaddle1();
    drawPaddle2();
    drawScore();
    // Activate
    setInterval(updateAll, 1000/fps);
};


//----------------------------------------------------------------
// move stuff around the screen
function updateAll() {
    drawCourt();
    drawPaddle1();
    drawPaddle2();
    drawBall();
    if (showingWinScreen) {
        drawScore();
        drawPauseScreen();
        return;
    }
    else {
        moveBall();
        cpuMovement();
    }
    drawScore();
    drawNet();
};


//----------------------------------------------------------------
// Game Controls
// Action on left click, ignore other
function detectLeftButton(evt) {
    if ("buttons") {
        return evt.buttons == 1;
    }
    var button = evt.which || evt.button;
    return button == 1;
};

// Restart the game
function handleMouseClick(evt) {
    if (detectLeftButton(evt) == 1) {
        if (showingWinScreen) {
            // reset scores
            p1Score = 0;
            p2Score = 0;
            // start a new game
            showingWinScreen = false
        }
    }
};

// Move paddle based on mouse position
function handleMouseMove(evt){
    let mousePos = calcMousePos(evt);
    // stop at top
    if (mousePos.y - (pad1Height / 2 + 20) <= 0){
        pad1Y = (pad1Height / 2) - 30;
    }
    // stop at bottom
    else if (mousePos.y + (pad1Height / 2 + 20) >= canvas.height){
        pad1Y = canvas.height - (pad1Height / 2 + 70);
    }
    // move paddle
    else {
        pad1Y = mousePos.y - (pad1Height / 2);
    }
};



//----------------------------------------------------------------
// Draw Game elements
// Draw the game court
function drawCourt() {
    let goalY = canvas.height / 2 - goalHeight / 2;
    // Outer rectangle
    colourRect(0, 0, canvas.width, canvas.height, "black");
    // Game Court
    colourRect(10, 10, (canvas.width - 20), (canvas.height - 20), "white");
    colourRect(20, 20, (canvas.width - 40), (canvas.height - 40), "black");
    // Goal Mouths
    colourRect(10, goalY, 10, goalHeight, "grey");
    colourRect(canvas.width - 20, goalY, 10, goalHeight, "grey");
};

function drawNet() {
    for (var i=20; i < canvas.height - 20; i+=40) {
        colourRect(canvas.width/2-1, i, 2, 20, "white");
    }
}

// Draw Ball
function drawBall() {
    colourCircle(ballX, ballY, ballR, "cyan");
};


// Draw player paddles
// Plasyer 1
function drawPaddle1() {
    colourRect(padOffset, pad1Y, padWidth, pad1Height, "red");
};
// CPU / Player 2
function drawPaddle2() {
    colourRect((canvas.width - padOffset - padWidth), pad2Y, padWidth, pad2Height, "blue");
};

function drawScore() {
    canvasContext.font = "20px Verdana";
    canvasContext.textAlign = "center"
    canvasContext.fillStyle = "red";
    canvasContext.fillText(p1Score, 100, 100);
    canvasContext.fillStyle = "blue";
    canvasContext.fillText(p2Score, canvas.width -100, 100);
};

function drawPauseScreen() {
    if (p1Score >= winningScore) {
        canvasContext.fillStyle = 'red';
        canvasContext.font = "30px Verdana";
        canvasContext.textAlign = "center"
        canvasContext.fillText("Red Wins", (canvas.width / 2), (canvas.height / 2 - 100));
    }
    else if (p2Score >= winningScore) {
        canvasContext.fillStyle = 'blue';
        canvasContext.font = "30px Verdana";
        canvasContext.textAlign = "center"
        canvasContext.fillText("Blue Wins", (canvas.width / 2), (canvas.height / 2 - 100));
    }
    canvasContext.fillStyle = 'white';
    canvasContext.font = "30px Verdana";
    canvasContext.textAlign = "center"
    canvasContext.fillText("Click to continue", (canvas.width / 2), (canvas.height / 2));
};


//----------------------------------------------------------------
// Control Game Elements
// Ball movement
function moveBall() {
    ballX += ballSpeedX;
    ballY += ballSpeedY;
    let goalY = canvas.height / 2 - goalHeight / 2;

    // Left Court
    if (ballX <= padOffset + padWidth + ballR && ballY > pad1Y && ballY < pad1Y + pad1Height) {
        // change ball direction
        ballSpeedX *= -1;
        let deltaY = ballY - (pad1Y + (pad1Height / 2));
        // set ball speed based on paddle impact location
        ballSpeedY = deltaY * 0.5;
    }
    if (ballX <= 20 + ballR){
        if (ballY < goalY || ballY > goalY + goalHeight) {
            // change ball direction when hitting court left edge
            ballSpeedX *= -1;
        }
        else {
            // Goal! increase P2 score and then reset ball
            p2Score++;
            ballReset();
        }
    }

    // Right Court
    if (ballX >= canvas.width - (padOffset + padWidth + ballR) && ballY > pad2Y && ballY < pad2Y + pad2Height) {
        // change ball direction
        ballSpeedX *= -1;
        // set ball speed based on paddle impact location
        let deltaY = ballY - (pad2Y + (pad2Height / 2));
        ballSpeedY = deltaY * 0.5;
    }
    if (ballX >= canvas.width - (20 + ballR)){
        if (ballY < goalY || ballY > goalY + goalHeight) {
            // change ball direction when hitting court right edge
            ballSpeedX *= -1;
        }
        else {
            // Goal! increase P1 score and then reset ball
            p1Score++;
            ballReset();
        }
    }

    // Y Axis
    // Top Court
    if (ballY >= canvas.height - (20 + ballR)) {
        ballSpeedY *= -1;
    }
    // Bottom Court
    if (ballY <= (20 + ballR)) {
        ballSpeedY *= -1;
    }
};

// Ball Reset
function ballReset() {
    if (p1Score >= winningScore || p2Score >= winningScore) {
        showingWinScreen = true;
    }
    ballX = canvas.width / 2;
    ballY = canvas.height / 2;
    ballSpeedX = 10;
    ballSpeedY = 0;
};


// paddle movement
function calcMousePos(evt) {
    let rect = canvas.getBoundingClientRect();
    let root = document.documentElement;
    let mouseX = evt.clientX - rect.left - root.scrollLeft;
    let mouseY = evt.clientY - rect.top - root.scrollTop;
    return {
        x: mouseX,
        y: mouseY
    };
};


// CPU player movement
function cpuMovement() {
    let pad2C = pad2Y + (pad2Height/2);

    // move paddle
    // move down
    if(pad2C < ballY - 35){
        // stop at bottom
        if (pad2C >= canvas.height - (pad2Height/2 + 20)){
            pad2Y = canvas.height - (pad2Height + 20);
        }
        else {
            pad2Y += 6;
        }
    }
    // move up
    else if (pad2C > ballY + 35) {
        // stop at top
        if (pad2C <= (pad2Height/2 + 20)){
            pad2Y = 20;
        }
        else {
            pad2Y -= 6
        }
    }
}


//----------------------------------------------------------------
// Draw Functions
// Generic Rectangle
function colourRect(leftX, topY, width, height, drawColour) {
    canvasContext.fillStyle = drawColour;
    canvasContext.fillRect(leftX, topY, width, height);
};


// generic Circle
function colourCircle(cX, cY, r, drawColour) {
    canvasContext.fillStyle = drawColour;
    canvasContext.beginPath();
    canvasContext.arc(cX, cY, r, 0, Math.PI*2, true);
    canvasContext.fill();
}
